<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class AppFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");


        for ($i=0; $i < 50; $i++) { 
            $article = new Article();
            $article
                ->setTitle($faker->sentence(rand(2,4)))
                ->setSlug($faker->sentence(rand(10,20)))
                ->setAuthor($faker->name($gender = 'male'|'female'))
                ->setContent($faker->paragraph($nbSentences = 50, $variableNbSentences = true))
                ->setPicture("https://picsum.photos/720/480?random=". random_int(1, 9999))
                ->setIsPublished($faker->boolean())
            ;
            $manager->persist($article);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
