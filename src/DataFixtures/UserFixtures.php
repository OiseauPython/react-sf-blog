<?php

namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
  /** @var UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
       $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setRoles(['ROLE_ADMIN'])
            ->setEmail("admin@react-sf-blog.com")
            ->setPassword($this->encoder->encodePassword($user, 'admin'))
            ;
        $manager->persist($user);

        $user = new User();
        $user
            ->setRoles(['ROLE_USER'])
            ->setEmail("user@react-sf-blog.com")
            ->setPassword($this->encoder->encodePassword($user, 'user'))
        ;
        $manager->persist($user);

        $manager->flush();
        $this->setReference('user', $user);
    }
}
