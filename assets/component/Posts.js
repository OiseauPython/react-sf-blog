import React, { Component } from "react";
import axios from "axios";
import PostView from "./PostView";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import './Posts.scss'

class Posts extends Component {
	constructor() {
		super();

		this.state = { posts: [], loading: true };
  }
  
  //Cette fois ci, on test l'appel à l'api dans une classe, sans hooks

	componentDidMount() {
		this.getPosts();
	}

  getPosts() {
    var url = "https://127.0.0.1:8000/api/articles/";
		axios
			.get(url, {
				headers: {
					Accept: "application/json",
				},
			})
			.then((res) => {
				const posts = res.data;
				this.setState({ posts, loading: false });
			});
	}

	render() {
		const loading = this.state.loading;
		return (
			<div>
				<section className="row-section">
					<div>
						<div className="">
							<h2 className="text-center">
								<span>The Jeremy Post</span>Des articles de
								qualité, essentiellement (de temps en temps).
							</h2>
						</div>

						{loading ? (
							<div className={"row text-center"}>
								<span className="fa fa-spin fa-spinner fa-4x"></span>
							</div>
						) : (
							<div>
								{this.state.posts.map((post) => (
									<main key={post.id} className="concord">
										<header>
											<time>
												<h5 id="date">January 30</h5>
												<h3 id="day">Today</h3>
											</time>
										</header>
										<div className="card xl">
											<section className="wrapper">
												<img
													style={{
														backgroundImage: `url(${post.picture})`,
													}}
													alt=""
												></img>
												<footer className="card-footer">
													<h6>Article</h6>
													<h4>{post.title}</h4>
													<p>{post.slug}</p>
													<Button
														as={Link}
														to={
															"/post-view/id=" +
															post.id
														}
													>
														Lire la suite
													</Button>
												</footer>
											</section>
											<div className="content"></div>
										</div>
										<div className="card x" data-color="">
											<section className="wrapper">
												<img
													style={{
														backgroundImage: `url(${post.picture})`,
														backgroundColor:
															"#ffcbb6",
													}}
													alt=""
												></img>
												<header className="card-title">
													<h6>Plus d'infos</h6>
													<h4>{post.title}</h4>
												</header>
												<footer className="card-footer">
													<p>{post.slug}</p>
												</footer>
											</section>
										</div>
									</main>
								))}
							</div>
						)}
					</div>
				</section>
			</div>
		);
	}
}

export default Posts;

