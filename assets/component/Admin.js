import React, { Component } from "react";
import axios from "axios";
import { FaBeer } from "react-icons/fa";
import { Link } from "react-router-dom";
import {  Button } from "react-bootstrap";
import './Admin.scss'

class Admin extends Component {
	constructor() {
		super();
		this.state = {
			users: [],
			articles: [],
			loadingArticles: true,
			loadingUsers: true,
		};
	}

	componentDidMount() {
		this.getUsers();
		this.getArticle();
  }
  
	getArticle() {
		var url = "https://127.0.0.1:8000/api/articles/";
		axios
			.get(url, {
				headers: {
					Accept: "application/json",
				},
			})
			.then((res) => {
				const articles = res.data;
				this.setState({ articles, loadingArticles: false });
			});
	}

	getUsers() {
		var url = "https://127.0.0.1:8000/api/users/";
		axios
			.get(url, {
				headers: {
					Accept: "application/json",
				},
			})
			.then((res) => {
				const users = res.data;
				this.setState({ users, loadingUsers: false });
			});
	}

	deleteRow(id) {
		axios.delete("https://127.0.0.1:8000/api/articles/" + id);
  }

	render() {
		return (
			<div>
				<section className="admin-row-section">
					{this.state.loadingUsers && this.state.loadingArticles ? (
						<div className={"row text-center"}>
							<span className="fa fa-spin fa-spinner fa-4x"></span>
						</div>
					) : (
						<div className="tabContainer">
							<table className="usersList">
								<thead>
									<tr>
										<th>ID User</th>
										<th>Email</th>
										<th>Rôle</th>
										<th>Éditer</th>
										<th>Supprimer</th>
									</tr>
								</thead>
								{this.state.users.map((user) => (
									<tbody>
										<tr>
											<td>{user.id}</td>
											<td>{user.email}</td>
											<td>{user.roles}</td>
											<td>
												<FaBeer />
											</td>
											<td>
												<FaBeer />
											</td>
										</tr>
									</tbody>
								))}
							</table>
							<Button as={Link} to={"/admin/add"}>
								
								Créer un article
							</Button>
							<table className="articlesList">
								<thead>
									<tr>
										<th>ID Article</th>
										<th>Nom Article</th>
										<th>Éditer</th>
										<th>Supprimer</th>
									</tr>
								</thead>
								{this.state.articles.map((post) => (
									<tbody>
										<tr>
											<td>{post.id}</td>
											<td>{post.title}</td>
											<td>
												<FaBeer />
											</td>
											<td>
												<button
													className="btn btn-danger"
													onClick={() =>
														this.deleteRow(post.id)
													}
												>
													Supprimer
												</button>
											</td>
										</tr>
									</tbody>
								))}
							</table>
						</div>
					)}
				</section>
			</div>
		);
	}
}
export default Admin;
