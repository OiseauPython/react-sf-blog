import React, { Component } from "react";
import axios from "axios";
import { FaBeer } from "react-icons/fa";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

class AddArticle extends Component {
	constructor() {
		super();
		this.state = {
			users: [],
			articles: [],
			loadingArticles: true,
			loadingUsers: true,
		};
	}

	onTitleChange = (e) => {
		this.setState({
			title: e.target.value,
		});
	};

	onContentChange = (e) => {
		this.setState({
			body: e.target.value,
		});
	};
	handleSubmit = (e) => {
		e.preventDefault();
		const data = {
			title: this.state.title,
			body: this.state.body,
		};
		axios
			.post("https://jsonplaceholder.typicode.com/posts", data)
			.then((res) => console.log(res))
			.catch((err) => console.log(err));
	};

	render() {
		return (
			<div>
				<div className="post">
					<form className="post" onSubmit={this.handleSubmit}>
						<input
							placeholder="Title"
							value={this.state.title}
							onChange={this.onTitleChange}
							required
						/>
						<textarea
							placeholder="Content"
							value={this.state.body}
							onChange={this.onBodyChange}
							required
						/>
						<button type="submit">Create Post</button>
					</form>
				</div>
			</div>
		);
	}
}
export default AddArticle;
