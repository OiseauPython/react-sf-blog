import React, { Component } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./PostView.scss";

class PostView extends Component {
	constructor() {
		super();

		this.state = { posts: [], loading: true };
	}

	getConcertById = (id) => {
    var url = "https://127.0.0.1:8000/api/articles/" + id;
	  axios
		.get(url, {
			headers: {
				Accept: "application/json",
			},
		})
		.then((res) => {
			const posts = res.data;
			this.setState({ posts, loading: false });
		});
  }

  getIdUrl() {
    var url2 = window.location.href; // or window.location.href for current url
    var captured = /id=([^&]+)/.exec(url2)[1]; // Value is in [1] ('384' in our case)
    var result = captured ? captured : "myDefaultValue";
    console.log(result);
    this.getConcertById(result)
  }

  render() {
    this.getIdUrl();
		const loading = this.state.loading;
		return (
			<div>
				<section className="row-section">
					<div className="">
						<h2 className="text-center">
							<span>{this.state.posts.title}</span>
							<p>Écrit par {this.state.posts.author} le</p>
						</h2>
					</div>
				</section>
				{loading ? (
					<div className={"row text-center"}>
						<span className="fa fa-spin fa-spinner fa-4x"></span>
					</div>
				) : (
					<section className="row-section">
						<main
							key={this.state.posts.id}
							className="concord mainView"
						>
							<div className="contentView">
								<p className="content">
									{this.state.posts.content}
								</p>
							</div>
							<div className="card x" data-color="">
								<section className="wrapper">
									<img
										style={{
											backgroundImage: `url(${this.state.posts.picture})`,
											backgroundColor: "#ffcbb6",
										}}
										alt=""
									></img>
									<header className="card-title">
										<h6>L'article</h6>
										<h4>{this.state.posts.title}</h4>
									</header>
									<footer className="card-footer">
										<p>{this.state.posts.slug}</p>
									</footer>
								</section>
							</div>
						</main>
					</section>
				)}
			</div>
		);
	}
}

export default PostView;
