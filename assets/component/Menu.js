import React, { Component } from "react";
import { Route, Switch, Redirect, Link, withRouter } from "react-router-dom";

class Menu extends Component {
	render() {
		console.log("je passe ici");
		return (
			<nav className="navbar navbar-expand-lg">
				<Link className={"navbar-brand"} to={"/"}>
					{" "}
					Un blog fait en React, Symfony et avec ❤️{" "}
				</Link>
				<div className="collapse navbar-collapse" id="navbarText">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link className={"nav-link"} to={"/posts"}>
								{" "}
								Posts{" "}
							</Link>
						</li>

						<li className="nav-item">
							<Link className={"nav-link"} to={"/admin"}>
								{" "}
								Admin{" "}
							</Link>
						</li>

						<li className="nav-item right">
							<Link className={"nav-link"} to={"/admin"}>
								{" "}
								Login{" "}
							</Link>
						</li>
					</ul>
				</div>
			</nav>
		);
	}
}

export default Menu;
