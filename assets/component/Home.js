import React, { Component } from "react";
import { Route, Switch, Redirect, Link, withRouter } from "react-router-dom";
import './Home.scss'
import SuperModal from './SuperModal'

class Home extends Component {
	render() {
		console.log("je passe ici");
		return (
			<div>
				<section className="accueilSection">
					<h1 className="bienvenu">👋  &#123;hello.world&#125; !</h1>
					<h4>
						Vous pouvez aller{" "}
						<Link className={"nav-link"} to={"/posts"}>
							{" "}
							voir de supers articles
						</Link>
						, ou{" "}
						<Link className={"nav-link"} to={"/admin"}>
							{" "}
							jouer les administrateurs{" "}
						</Link>
					</h4>
					<br />
					<SuperModal />
				</section>
			</div>
		);
	}
}

export default Home;
