import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";

class SuperModal extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);

		this.state = {
			show: false,
		};
	}

	handleClose() {
		this.setState({ show: false });
	}

	handleShow() {
		this.setState({ show: true });
	}

	render() {
		return (
			<>
				<Button variant="primary" onClick={this.handleShow}>
					Le petit compte rendu
				</Button>

				<Modal show={this.state.show} onHide={this.handleClose}>
					<Modal.Header closeButton>
						<Modal.Title>
							React, c'est cool (quand on sait faire)
						</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						Bon, il est bientot minuit, pas mal de frustration car
						d'un côté je suis allé bien plus vite que le Concert,
						mais pas assez pour faire le "minimum" de
						fonctionnalités d'un blog. Il n'y a donc pas de
						sécurité, comme marqué sur l'accueil, n'importe qui peut
						jouer à l'administrateur. Aussi, je n'ai réalisé que le
						listing et la suppression des articles (il faut actualiser la page, c'est pas sexy, mais mon moyen react ma fait planter plusieurs fois). le post, c'est
						pour bientot normalement, le put, je l'ai en tête mais
						y'a encore du boulot. J'ai commencé à toucher au JWT,
						mais j'ai pris que 1h pour regarder, trop peur de perdre
						tout mon temps. J'ai quand même voulu prendre un peu de
						temps pour le design, c'est quand même plus sympa de
						bosser sur un site agréable à l'oeil (épuré en tout
						cas).
						<br />
						Le TP solo était une bonne experience de course contre
						la montre (une première pour moi !)
						<br />
						Merci
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={this.handleClose}>
							Fermer
						</Button>
						<Button variant="primary" onClick={this.handleClose}>
							Fermer, mais en bleu
						</Button>
					</Modal.Footer>
				</Modal>
			</>
		);
	}
}

export default SuperModal;
