/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route, HashRouter } from "react-router-dom";
import Menu from "./component/Menu";
import Home from "./component/Home";
import Admin from "./component/Admin";
import Posts from "./component/Posts";
import PostView from "./component/PostView";
import AddArticle from "./component/AddArticle";


function App() {
	return (
		<HashRouter>
			<Menu />

			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/admin" component={Admin} />
				<Route path="/admin/add" component={AddArticle} />
				<Route path="/posts" component={Posts} />
				<Route path="/post-view" component={PostView} />
			</Switch>
		</HashRouter>
	);
}


ReactDOM.render(
		<App />,
	document.getElementById("root")
);